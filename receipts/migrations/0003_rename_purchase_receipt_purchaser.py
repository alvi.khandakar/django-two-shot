# Generated by Django 4.1.2 on 2022-10-20 00:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_receipt_account"),
    ]

    operations = [
        migrations.RenameField(
            model_name="receipt",
            old_name="purchase",
            new_name="purchaser",
        ),
    ]
